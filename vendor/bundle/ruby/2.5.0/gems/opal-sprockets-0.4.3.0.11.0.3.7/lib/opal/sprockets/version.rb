module Opal
  module Sprockets
    BASE_VERSION = '0.4.3'
    OPAL_VERSION = '0.11.0'
    SPROCKETS_VERSION = '3.7'
    VERSION = "#{BASE_VERSION}.#{OPAL_VERSION}.#{SPROCKETS_VERSION}"
  end
end
