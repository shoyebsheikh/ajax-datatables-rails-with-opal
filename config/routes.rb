Rails.application.routes.draw do
  get 'user/index'
  get 'hello_world/index'
  post '/data.json', :to => redirect('/data.json')
  post '/user/index.json', :to => redirect('/user/index.json')
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
