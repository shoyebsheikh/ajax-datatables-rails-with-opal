Document.ready? do
  token     = Element['meta[name=csrf-token]'].attr('content');
  settings  = {
      "ajax": {
        "url": Element['#tree-datatable'].data('source'),
        "type": 'POST',
        "beforeSend": lambda do |xhr|
          `xhr.setRequestHeader('X-CSRF-Token', token)`
        end 
      },
      "pagingType": "full_numbers",
      "destroy": true,
    "columns": [
      {"data": "id"},
      {"data": "first_name"},
      {"data": "last_name"},
      {"data": "email"}
    ]
    }
  Element['#tree-datatable'].dataTable(settings.to_n)

end
