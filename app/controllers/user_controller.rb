class UserController < ApplicationController
  def index
    params["columns"] ||= { "0" => {"data" => "" } }
    params["length"]  ||= -1
    respond_to do |format|
      format.html
      format.json { render json: UserDatatable.new(params) }
    end
  end  
end
